import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'package:teman_belajar/component/page/genColor.dart';
import 'package:teman_belajar/component/page/genPage.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:teman_belajar/component/page/genText.dart';

class Dashboard extends StatefulWidget {
  @override
  _DashboardState createState() => _DashboardState();
}

class _DashboardState extends State<Dashboard> {
  @override
  void initState() {
    // TODO: implement initState
//
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context, width: 750, height: 1334, allowFontScaling: true);

    return GenPage(
      statusBarColor: GenColor.primaryColor,
      appbar: Container(
          width: double.infinity,
          height: 100.h,
          color: GenColor.primaryColor,
          child: Stack(
            children: <Widget>[
              Container(
                width: 100.w,
                child: Center(
                    child: InkWell(
                  child: Icon(
                    Icons.menu,
                    color: Colors.white,
                    size: 50.h,
                  ),
                )),
              ),
              Center(
                child: Image.asset(
                  "assets/images/logo.png",
                  height: 60.h,
                ),
              )
            ],
          )),
      body: Container(
        width: 1.wp,
        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              Container(
                padding: EdgeInsets.only(bottom: 40.h),
                decoration: BoxDecoration(
                    color: GenColor.primaryColor,
                  borderRadius: new BorderRadius.vertical(
                      bottom: new Radius.elliptical(
                          MediaQuery.of(context).size.width, 55.0)),
                ),
                child: Column(
                  children: <Widget>[
                    SizedBox(
                      height: 60.h,
                    ),
                    GenText(
                      "Hybrid Learning",
                      style: TextStyle(fontSize: 60.ssp, color: Colors.white),
                    ),
                    SizedBox(
                      height: 20.h,
                    ),
                    Padding(
                      padding: EdgeInsets.all(32.w),
                      child: GenText(
                        "Belajar jadi lebih mudah dengan metode hybrid learning yang mengintegrasikan pembelajaran online dan offline (tatap muka). ",
                        style: TextStyle(fontSize: 30.ssp, color: Colors.white),
                        textAlign: TextAlign.center,
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: 32.w, left: 32.w, right: 32),
                      child: GenText(
                        "Belajar, Berlatih, Berprestasi",
                        style: TextStyle(fontSize: 30.ssp, color: Colors.white),
                        textAlign: TextAlign.center,
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: 32.w),
                      child: GenText(
                        "#PintarDenganTemanBelajar",
                        style: TextStyle(fontSize: 30.ssp, color: Colors.white, fontWeight: FontWeight.bold),
                        textAlign: TextAlign.center,
                      ),
                    ),
                    SizedBox(
                      height: 20.h,
                    ),
                  ],
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: 50.w, right: 16.w, left: 16.w),
                child: GenText(
                  "Temukan Guru Les Privat Terbaikmu",
                  style: TextStyle(fontSize: 45.ssp, color: Colors.black), textAlign: TextAlign.center,
                ),
              ),
            ],
          ),

        ),
      ),
    );
  }
}
