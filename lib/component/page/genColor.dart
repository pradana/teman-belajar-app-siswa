import 'package:flutter/material.dart';

class GenColor {
  static const primaryColor = Color.fromARGB(255, 29, 161, 242);
  static const accentColor = Color.fromRGBO(239, 67, 53, 1);
  static const starColor = Color.fromRGBO(228,173, 75, 1);
  static const shimmer = Color.fromARGB(15, 0, 0, 0);
  static const backGroundColor = Color.fromARGB(1, 12,43,56);
}
