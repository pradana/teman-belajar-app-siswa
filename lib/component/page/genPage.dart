import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:teman_belajar/component/page/genColor.dart';


class GenPage extends StatelessWidget {
  final Color statusBarColor;
  final Widget sidebarDrawer;
  final Widget body;
  final Widget appbar;
  final GlobalKey genKey;
  final Map routes;
  final BottomNavigationBar bottomNavigationBar;

  GenPage(
      {this.statusBarColor,
      this.genKey,
      this.sidebarDrawer,
      this.body,
      this.appbar,
      this.routes,
      this.bottomNavigationBar});




  @override
  Widget build(BuildContext context) {



    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      statusBarColor: statusBarColor, // status bar color
    ));

    Size size = MediaQuery.of(context).size;
    ScreenUtil.init(context);
    ScreenUtil.init(context, width: size.width, height: size.height);

    return Scaffold(
      key: genKey,
      drawer: sidebarDrawer,
      body: SafeArea(
        child: Column(
          children: <Widget>[
            appbar == null
                ? Container()
                : Container(
                    color: statusBarColor ?? GenColor.primaryColor,
                    child: appbar
                  ),
            Expanded(
              child: body,
            ),
          ],
        ),
      ),
      bottomNavigationBar: bottomNavigationBar,
    );
    throw UnimplementedError();
  }
}
