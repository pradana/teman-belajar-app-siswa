import 'package:flutter/material.dart';

class GenDimen {
  static const double sidePadding = 16;
  static const double jarakCard = 16;
  static const double spaceSection = 50;
}
