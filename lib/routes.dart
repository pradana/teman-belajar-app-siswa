
import 'package:provider/provider.dart';
import 'package:teman_belajar/page/dashboard/Dashboard.dart';

import 'blocs/baseBloc.dart';

class GenProvider {
  static var providers = [
    ChangeNotifierProvider<BaseBloc>.value(value: BaseBloc())
  ];

  static routes(context) {

    return {

      '/': (context) {
        return Dashboard();
      },

    };
  }
}